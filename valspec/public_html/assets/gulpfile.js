'use strict';

var gulp = require('gulp'),
	browser = require('browser-sync').create(),
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	minijyJS = require('gulp-uglify'),
	concatJS = require('gulp-concat')


gulp.task('003', function() {
	gulp.watch('node_modules/inuitcss/**/*.scss',['sass']);
	gulp.watch('script/files/*.js', ['js'])
	});	


gulp.task('html', function() {
	return gulp
		.src('templates/*.html')
		.pipe(gulp.dest('dist'));
});


gulp.task('sass', function() {
	return gulp
	.src('node_modules/inuitcss/*.scss')
	.pipe(sass({
		outputStyle: 'compressed'
	}))
	.pipe(autoprefixer({
		overrideBrowserslist: ['last 2 versions', 'ie 6-14']
		}))
	.pipe(gulp.dest('css/'));
	})


gulp.task('sass-uglify', function() {
	return gulp
	.src('node_modules/inuitcss/**/*.scss')
	.pipe(sass({
		outputStyle: 'condensed'
	}))
	.pipe(autoprefixer({
		overrideBrowserslist: ['last 2 versions', 'ie 6-14']
		}))
	.pipe(gulp.dest('css/uglify'));
	})


gulp.task('js', function() {
	return gulp
	.src('script/files/*.js')
	.pipe(concatJS('app.js'))
	.pipe(gulp.dest('script/'));
	})

gulp.task('js-uglify', function() {
	return gulp
	.src('script/files/*.js')
	.pipe(concatJS('app.js'))
	.pipe(gulp.dest('script/uglify/'));
	})


gulp.task('default', ['003','sass','sass-uglify','js','js-uglify']);		